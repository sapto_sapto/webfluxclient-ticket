package com.sapto.webflux.webclientflux.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;

@Service
public class FluxLatServices implements FluxLatService {

	Flux<Employee> employees = Flux.empty();

	public void generateFluxFromList() {
		List<Employee> employees = new ArrayList<>();
		for (int i = 1; i < 10; i++) {
			employees.add(new Employee(i, "firstName-" + i, "lastName-" + i, "title-" + i, "workPhone-" + i));
		}

		Flux<Employee> flEmployee = Flux.fromIterable(employees).filter(x -> x.getEmployeeID() > 2)
				.filter(p -> p.getEmployeeID() > 5);

		// flEmployee.subscribe(x -> System.out.println(x.getEmployeeID()));

		Flux<Children> flEmployee2 = Flux.fromIterable(employees).map(p->new Children(p.getFirstName()));

		flEmployee2.subscribe(p -> System.out.println(p.getFatherName()));

		//iterasi tiap order
		//dapatkan orderdetail dari tiap order
		//set ke orderdetail ke order
	}

}
